package com.employeeservice.service;

import java.util.List;

import javax.validation.Valid;

import com.employeeservice.dto.AddEmployeeServiceDto;
import com.employeeservice.dto.EmployeeDetailsDto;
import com.employeeservice.dto.UpdateEmployeeServiceDto;
import com.employeeservice.entity.EmployeeDetails;

public interface EmployeeService {

	/**
	 * 
	 * @param employeeDetailsDto
	 */
	void addEmployee(@Valid AddEmployeeServiceDto employeeDetailsDto);
	/**
	 * 
	 * @param updateEmployeeServiceDto
	 * @param employeeCode
	 * @return
	 * @throws Exception
	 */

	UpdateEmployeeServiceDto updateEmployee(UpdateEmployeeServiceDto updateEmployeeServiceDto, Long employeeCode)throws Exception;
	/**
	 * 
	 * @param employeeCode
	 * @return
	 */

	EmployeeDetails getEmployeeByCode(Long employeeCode);
	/**
	 * 
	 * @param employeeStatus
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */

	List<EmployeeDetailsDto> getAllEmployeeDetails(String employeeStatus, int pageNumber, int pageSize) throws Exception;
	/**
	 * 
	 * @param employeeCode
	 * @return
	 * @throws Exception
	 */

	String removeEmployee(Long employeeCode) throws Exception ;

}
